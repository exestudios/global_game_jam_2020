﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;
    public bool isWalking = false;
    public bool isShooting = false;
    public bool isJumping = false;

    public Sound[] allClips;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        foreach (Sound soundClip in allClips)
        {
            soundClip.source = gameObject.AddComponent<AudioSource>();
            soundClip.source.clip = soundClip.clip;
            soundClip.source.loop = soundClip.loop;
        }


    }
    void Start()
    {
        PlaySound("Music");
    }

    private void Update()
    {

        
        if (isWalking)
        {
            PlaySound("Walking");
            Debug.Log("calling walking");
        }
        if (!isWalking)
        {
            Stop("Walking");
            Debug.Log("stop walk");
        }


        if (isShooting)
        {
            PlaySound("Shoot");
            Debug.Log("Calling shoot");
        }
        if (!isShooting)
        {
            Stop("Shoot");
            Debug.Log("Stop shoot");
        }
  
    }

    public void PlaySound(string sound)
    {

        //Debug.Log("sound string: "+ sound);
        Sound s = Array.Find(allClips, item => item.name == sound);
        //Debug.Log("the value of S is: " + s.name);
        if (s == null)
        {
            Debug.LogError("Sound: " + name + " not found!");
            return;
        }
        if (s.name == "Music") {
            s.source.volume = 0.2f;
        }

        //Debug.Log("val of S: " + s);
        //Debug.Log("val of s.source: " + s.source);
        //Debug.Log("val of s.source.name: " + s.source.name);
        //Debug.Log("val of s. " + s.name);



        s.source.Play();
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(allClips, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

      
        s.source.Stop();
    }
}
