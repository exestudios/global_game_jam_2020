﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ValveManager : MonoBehaviour, IInteractable
{
    private Animator m_an;
    private float counter;

    private void Awake()
    {
        m_an = GetComponent<Animator>();
        if (m_an == null)
        {
            Debug.LogError("You forgot the animator dipshit");
        }
        m_an.SetBool("Sturdy", false);
    }

    private void Update()
    {
        
       
    }

    public void OnHit(float time)
    {
        if (counter != 0)
        {
            if (time - counter <= Time.deltaTime)
            {
                m_an.SetBool("Test", true);
                m_an.SetBool("CtrlPressed", true);
                if (m_an.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f && AnimatorNameIsPlaying("ValveTurn"))
                {
                    m_an.SetBool("Sturdy", true);
                }
            }
            else
            {
                m_an.SetBool("CtrlPressed", false);
            }
        }

        counter = time;

    }

    private bool AnimatorIsPlaying()
    {
        return m_an.GetCurrentAnimatorStateInfo(0).normalizedTime < 1;
    }
    private bool AnimatorNameIsPlaying(string stateName)
    {
        return AnimatorIsPlaying() && m_an.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
}