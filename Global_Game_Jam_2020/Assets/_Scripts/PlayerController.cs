﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [SerializeField] private float m_jump, m_speed, m_timer, m_coyoteTimeBase = 0.2f, m_jumpBufferBase = 0.2f, c_laserSize = 10;
    public bool p_faceLeft, p_activeShooting, p_dead;
    private float m_horizontal, m_activeCoyoteTime, m_activeJumpBuffer, m_maxAmmo;
    private Vector3 m_dir;
    private Transform c_playerSprite;
    public GameObject c_la;
    public GameManager p_gm;
    private Rigidbody2D m_rb;
    private Animator m_an;
    [SerializeField] private AudioManager p_am;

    void Awake()
    {

        p_dead = false;
        m_maxAmmo = p_gm.p_ammo;
        m_rb = GetComponent<Rigidbody2D>();
        if (m_rb == null)
        {
            Debug.LogError("No Rigidbody2D?  You monkey!");
        }

        c_playerSprite = transform.Find("PlayerSprite");
        if (c_playerSprite == null)
        {
            Debug.LogError("No child sprite?  You actual ape!");
        }

        c_la.GetComponent<SpriteRenderer>().enabled = false;

        m_an = GameObject.Find("PlayerSprite").GetComponent<Animator>();
        if (m_an == null)
        {
            Debug.LogError("No child playersprite, you are nothing less of a baboon");
        }




    }
    private void Update()
    {
        ResetBuffers();
        CountdownBuffers();
        CheckJump();
        ChangeDirectionBody();
        TrackMouse();
        CheckMouse();

        if (m_rb.velocity.y <= 0f)
        {
            GroundCheck();

        }

        if (Input.GetMouseButtonUp(0))
        {
            m_maxAmmo -= 0.05f;
            p_am.isShooting = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            p_am.isShooting = true;
        }



        if (!p_activeShooting && p_gm.p_ammo < m_maxAmmo)
        {

            p_gm.p_ammo = p_gm.p_ammo + 0.0005f;
        }



        if (m_horizontal != 0)
        {
            p_am.isWalking = true;
        
        }
        if (m_horizontal == 0)
        {
            p_am.isWalking = false ;
            
        }

        

        m_horizontal = Input.GetAxisRaw("Horizontal");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.GetMask("Death")) {
            Die();
        }
    }
    private void FixedUpdate()
    {
        m_rb.velocity = new Vector3(m_horizontal * m_speed, m_rb.velocity.y);
    }

    void Flip(int value)
    {
        if (value == 1)
        {
            p_faceLeft = true;

        }
        else
        {
            p_faceLeft = false;
        }

        c_playerSprite.localScale = new Vector2(value, c_playerSprite.localScale.y);
        c_la.transform.localPosition = new Vector2(0.4f * -value, 0.4f);


    }

    private bool GroundCheck()
    {
        return Physics2D.Raycast(m_rb.transform.position, Vector2.down, GetComponent<CapsuleCollider2D>().size.y, LayerMask.GetMask("Ground"));
    }

    private bool PlayerJumpInput()
    {
        if (Input.GetAxis("Jump") != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void CountdownBuffers()
    {
        m_activeCoyoteTime -= Time.deltaTime;
        m_activeJumpBuffer -= Time.deltaTime;
    }

    void CheckJump()
    {
        if (m_activeCoyoteTime >= 0 && m_activeJumpBuffer >= 0)
        {

            m_rb.velocity = new Vector3(m_rb.velocity.x, 0f, 0f);
            m_rb.AddForce(new Vector3(0, m_jump, 0), ForceMode2D.Impulse);
        }
    }

    void ChangeDirectionBody()
    {
        if (m_horizontal != 0)
        {

            m_an.SetBool("Moving", true);
            if (m_horizontal < 0)
            {
                Flip(1);
            }

            if (m_horizontal > 0)
            {
                Flip(-1);
            }
        }
        else
        {

            m_an.SetBool("Moving", false);
        }
    }


    void ResetBuffers()
    {
        if (GroundCheck())
        {
            m_activeCoyoteTime = m_coyoteTimeBase;
        }

        if (PlayerJumpInput())
        {
            m_activeJumpBuffer = m_jumpBufferBase;
        }
    }

    void CheckMouse()
    {
        if (Input.GetMouseButton(0) && m_rb.velocity.x == 0 && p_gm.p_ammo > 0.01f)
        {
            p_gm.p_ammo -= 0.001f;
            p_activeShooting = true;
            Shoot();
        }
        if (Input.GetMouseButtonUp(0) || m_rb.velocity.x != 0)
        {

            p_activeShooting = false;
            c_la.GetComponent<SpriteRenderer>().enabled = false;

        }
    }

    void TrackMouse()
    {
        m_dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        m_dir.Normalize();
        float flipValue = Mathf.Atan2(m_dir.y, m_dir.x) * Mathf.Rad2Deg;
        if (m_rb.velocity.x == 0)
        {

            if (Mathf.Abs(flipValue) > 90)
            {
                Flip(1);
            }
            else
            {
                Flip(-1);
            }
        }
    }
    void Shoot()
    {
        c_la.GetComponent<SpriteRenderer>().enabled = true;
        float tempAngle = Mathf.Atan2(m_dir.y, m_dir.x) * Mathf.Rad2Deg;
        c_la.transform.rotation = Quaternion.Euler(0f, 0f, tempAngle);

        c_laserSize = 5;

        RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), m_dir, c_laserSize, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            IInteractable interactable = hit.collider.gameObject.GetComponent<IInteractable>();

            if (interactable != null)
            {
                interactable.OnHit(Time.time);
            }
            c_laserSize = Vector2.Distance(hit.point, transform.position);
        }
        c_la.transform.localScale = new Vector2(c_laserSize, 1);



    }

    void Die() {
        p_dead = true;
        this.enabled = false;
    }



}

