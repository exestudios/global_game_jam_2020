﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Image m_ammoBar;
    public GameObject player;
    public float p_ammo = 1;

    void Update()
    {
       

        m_ammoBar.rectTransform.localScale = new Vector2(p_ammo, 1);
        if (player.GetComponent<PlayerController>().p_dead) {
            SceneManager.LoadScene(1);
        }
    }
}
