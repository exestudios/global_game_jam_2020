﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour, IInteractable
{
    private Animator m_an;
    public AudioManager p_am;
    private float counter;

    private void Awake()
    {
        m_an = GetComponent<Animator>();
        if (m_an == null)
        {
            Debug.LogError("You forgot the animator dipshit");
        }
        m_an.SetBool("Sturdy", false);
    }

    private void Update()
    {
        if (m_an.GetBool("Sturdy"))
        {
            GetComponent<BoxCollider2D>().offset = new Vector2(0, 1.8f);
            GetComponent<BoxCollider2D>().size = new Vector2(12.75f, 0.3f);
            p_am.Stop("BridgeSound");
        }
        m_an.SetBool("CtrlPressed", false);
    }

    public void OnHit(float time)
    {
        m_an.SetBool("CtrlPressed", true);
        if (counter != 0)
        {
            if (time - counter <= Time.deltaTime)
            {
                p_am.PlaySound("BridgeSound");
                m_an.SetBool("Test", true);
                
                if (m_an.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f && AnimatorNameIsPlaying("Bridge_Reconstruction"))
                {
                    m_an.SetBool("Sturdy", true);
                }
            }
            else
            {
                m_an.SetBool("CtrlPressed", false);
                p_am.Stop("BridgeSound");
            }
        }
        counter = time;
    }

    bool AnimatorIsPlaying()
    {
        return m_an.GetCurrentAnimatorStateInfo(0).normalizedTime < 1;
    }
    bool AnimatorNameIsPlaying(string stateName)
    {
        return AnimatorIsPlaying() && m_an.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
}
